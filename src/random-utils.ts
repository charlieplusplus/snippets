export function getRandomInRange(max: number): number {
  return Math.floor(Math.random() * Math.floor(max));
}
