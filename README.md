# README

A collection of TypeScript snippets.

### Contents
- Arrays
- Dates
- Files
- Objects
- Random (mathematical)


### Tests
To run unit tests for these snippets, run `npm test`.
